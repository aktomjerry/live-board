export const modalTypes = {
  NEW_ORDER: 'NEW_ORDER',
  ERROR: 'ERROR',
  ALERT: 'ALERT',
  INFO: 'INFO',
  CONFIRMATION : 'CONFIRMATION'
};

export const orderKeys={
    ORDER_ID : 'orderId',
    SELECTED_USER:'selectedUser', 
    ORDER_TYPE : 'orderType', 
    PRICE_PER_KG : 'pricePerKg',
    QUANTITY : 'quantity'
}