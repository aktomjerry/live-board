import React from 'react';
import {
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Typography,
  IconButton,
} from '@material-ui/core';

import {Close} from '@material-ui/icons';

export default function OrderTable(props) {
  const cancelOrderHandler = (e) => {
    props.cancelOrderHandler(JSON.parse(e.currentTarget.value)); 
  };

  return (
    <TableContainer component={Paper} className="tbl-container">
      <Typography variant="h6" align="left">
        {props.title || 'Title'}
      </Typography>
      <Table className="" aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Order Type</TableCell>
            <TableCell align="right">Qty&nbsp;(kg)</TableCell>
            <TableCell align="right"></TableCell>
            <TableCell align="right">Price </TableCell>
            <TableCell className="order-lists" align="right">
              Order Id(s)
            </TableCell>
            <TableCell>#</TableCell>
          </TableRow>
        </TableHead>  
        <TableBody>
          {Array.isArray(props.data) && props.data.length? (
            props.data.map((row) => (
              <TableRow key={row.id}>
                <TableCell>{props.title}</TableCell>
                <TableCell align="right">{`${row.quantity.toFixed(
                  2
                )} kg`}</TableCell>
                <TableCell align="right">{'for'}</TableCell>
                <TableCell align="right">{`£ ${row.pricePerKg.toFixed(
                  2
                )}`}</TableCell>
                <TableCell align="right">{row.idList.join(' , ')}</TableCell>
                <TableCell>
                  <IconButton
                    onClick={cancelOrderHandler}
                    value={JSON.stringify(row.idList)}
                  >
                    <Close />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))
          ) : (
            <TableRow>
              <TableCell colSpan="6">
                <Typography className="no-data">No Data</Typography>
              </TableCell>
            </TableRow>
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
