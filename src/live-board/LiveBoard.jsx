import React, { Component } from 'react';
import { Paper, AppBar, Toolbar, Typography, Button } from '@material-ui/core';
import OrderTable from './OrderTable';
import './live-board.scss';
import { modalTypes } from '../constants';
import {v4 as uuid4} from 'uuid';

const tableType = {
  SELL: 'Sell',
  BUY: 'Buy',
};

export default class LiveBoard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  getInfoObject = (rawInfo) => {
    return {
      ...rawInfo,
      idList: rawInfo.orders.join(','),
    };
  };

  getTable = (type) => {
    return (
      <OrderTable
        title={type}
        data={this.shortData(this.getTableData(type), type === 'Buy')}
        cancelOrderHandler={this.cancelOrderHandler}
      />
    );
  };

  shortData = (arrData, desc = false) => {
    // for descending order 
    if (desc) return arrData.sort((a, b) => b.pricePerKg - a.pricePerKg);
    else return arrData.sort((a, b) => a.pricePerKg - b.pricePerKg); // for ascending order
  };

  getTableData = (dataType) => {
    let finalData = [];
    const { orderData } = this.props;
    if (!orderData || !dataType) return null;

    //grouping data via price
    if (dataType === tableType.BUY) {
      finalData = Object.entries(orderData)
        .filter(([key, order]) => order.orderType === 'Buy')
        .reduce((acc, [key, order], i, col) => {
          order.orderId = key;
          acc[order.pricePerKg] = [...(acc[order.pricePerKg] || []), order];
          return acc;
        }, {});
    } else {
      finalData = Object.entries(orderData)
        .filter(([key, order]) => order.orderType === 'Sell')
        .reduce((acc, [key, order], i, col) => {
          order.orderId = key;
          acc[order.pricePerKg] = [...(acc[order.pricePerKg] || []), order];
          return acc;
        }, {});
    }

    //merging the grouped data to calculate new quantity and order ids
    return this.getMergedData(finalData);
  };

  getMergedData = (orderList) => {
    let mergedData = Object.entries(orderList).reduce(
      (acc, [key, valArr], i) => {
        const newObj = valArr.reduce(
          (a, o, i) => {
            a.id = uuid4();
            a.quantity += parseFloat(o.quantity);
            a.idList = Array.isArray(a.idList)
              ? [...a.idList, o.orderId]
              : [o.orderId];
            a.pricePerKg = parseFloat(o.pricePerKg);
            return a;
          },
          { quantity: 0, idList: [], pricePerKg: 0 }
        );
        acc.push(newObj);
        return acc;
      },
      []
    );
    return mergedData;
  };

  newOrderHandler = () => {
    const modalProps = {
      modalType: modalTypes.NEW_ORDER,
      newOrderHandler: this.props.addNewOrder,
    };
    this.props.showModal(modalProps);
  };

  cancelOrderHandler = (orderIdList) => {
    if (Array.isArray(orderIdList)) {
      const modalProps = {
        title: 'Order Cancellation',
        body:
          'You are about to cancel an order , are you sure you want to proceed ?',
        modalType: modalTypes.CONFIRMATION,
        handleConfirmation: this.props.cancelOrder,
        confirmationProps: orderIdList,
      };
      this.props.showModal(modalProps);
    }
  };

  render() {
    return (
      <Paper className="board-container">
        <header className="board-header">
          <AppBar position="static">
            <Toolbar>
              <Typography variant="h5">
                {this.props.title || 'Title Here'}
              </Typography>
            </Toolbar>
          </AppBar>
        </header>

        <div className="board-body">
          <div className="button-row">
            <Button
              variant="contained"
              color="primary"
              className="btn-new-order"
              onClick={this.newOrderHandler}
            >
              New Order
            </Button>
          </div>

          <div className="tbl-sell tbl">{this.getTable(tableType.SELL)}</div>

          <div className="tbl-buy tbl">{this.getTable(tableType.BUY)}</div>
        </div>
      </Paper>
    );
  }
}
