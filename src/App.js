import React from 'react';
import logo from './logo.svg';
import { v4 as uuidv4 } from 'uuid';
import LiveBoard from './live-board/LiveBoard';
import Modal from './modal/Modal';
import { users } from './dummy_data/dummy_users';
import { orders } from './dummy_data/dummy_orders';
import './App.css';
import { modalTypes } from './constants';

const getInitialState = () => {
  return { users, orders, modalProps: { showModal: false } };
};

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = getInitialState();
  }

  hideModal = () => {
    this.setState(() => ({
      modalProps: { showModal: false },
    }));
  };

  showModal = (properties) => {
    this.setState(() => ({ modalProps: { showModal: true, ...properties } }));
  };

  generateNewOrderId = () => {
    return uuidv4();
  };

  addNewOrder = (newOrder) => {
    try {
      if (newOrder) {
        let orders = { ...this.state.orders };
        orders[this.generateNewOrderId()] = newOrder;
        const modalProps = {
          showModal: true,
          title: 'Success',
          body: 'Successfully added the new order',
          modalType: modalTypes.INFO,
        };
        this.setState(() => ({ orders, modalProps }));
      }
    } catch (err) {
      console.log(err.message);
    }
  };

  cancelOrder = (orderIdList) => {
    try {
      if (Array.isArray(orderIdList)) {
        let orders = { ...this.state.orders };

        orderIdList.forEach((orderId) => {
          delete orders[orderId];
        });
        const modalProps = {
          showModal: true,
          title: 'Success',
          body: 'Successfully Cancelled the order',
          modalType: modalTypes.INFO,
        };
        this.setState(() => ({ orders, modalProps }));
      }
    } catch (err) {
      console.log(err.message);
    }
  };

  render() {
    return (
      <div className="App">
        <Modal
          {...this.state.modalProps}
          hideModal={this.hideModal}
          userList={this.state.users}
        />
        <LiveBoard
          title={'Silver Bars Marketplace : Live Board'}
          addNewOrder={this.addNewOrder}
          cancelOrder={this.cancelOrder}
          userData={this.state.users}
          orderData={this.state.orders}
          showModal={this.showModal}
        />
      </div>
    );
  }
}
