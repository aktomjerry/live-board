import React, { Fragment } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core';
import { modalTypes, orderKeys } from '../constants';
import NewOrderForm from './NewOrderForm';
import './modal.scss';

const getInitialState = () => ({
  [orderKeys.SELECTED_USER]: '',
  [orderKeys.QUANTITY]: 0.0,
  [orderKeys.PRICE_PER_KG]: 0.0,
  [orderKeys.ORDER_TYPE]: 'Buy',
});

export default class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.state = getInitialState();
  }

  handleClose = () => {
    this.setState(
      () => getInitialState(),
      () => {
        this.props.hideModal();
      }
    );
  };

  handleConfirm = () => {
    if (this.props.handleConfirmation) {
      if (this.props.confirmationProps)
        this.props.handleConfirmation(this.props.confirmationProps);
      else this.props.handleConfirmation();
    }
    this.handleClose();
  };

  handleNewOrder = () => {
    if (this.props.newOrderHandler) {
      const order = { ...this.state };
      this.props.newOrderHandler(order);
    }
    //this.handleClose();
  };

  getModalTitle = () => {
    return this.props.modalType === modalTypes.NEW_ORDER
      ? 'Add new order'
      : this.props.title
      ? this.props.title
      : 'Modal Title Here';
  };

  getModalBody = () => {
    switch (this.props.modalType) {
      case modalTypes.NEW_ORDER:
        return this.getNewOrderForm();
      default:
        return (
          <DialogContentText id="alert-dialog-description">
            {this.props.body || 'Modal body here'}
          </DialogContentText>
        );
    }
  };

  getModalActions = () => {
    const { modalType } = this.props;
    switch (modalType) {
      case modalTypes.NEW_ORDER:
        return (
          <Fragment>
            <Button onClick={this.handleNewOrder} color="primary">
              Add order
            </Button>
            <Button
              onClick={this.handleClose}
              color="default"
              variant="outlined"
            >
              Cancel
            </Button>
          </Fragment>
        );
      case modalTypes.CONFIRMATION:
        return (
          <Fragment>
            <Button onClick={this.handleConfirm} color="primary">
              Confirm
            </Button>
            <Button
              onClick={this.handleClose}
              color="default"
              variant="outlined"
            >
              Cancel
            </Button>
          </Fragment>
        );
      default:
        return (
          <Button onClick={this.handleClose} color="primary">
            OK
          </Button>
        );
    }
  };

  updateOrderInfo = (key, value) => {
    this.setState(() => ({ [key]: value }));
  };

  getNewOrderForm = () => {
    const { userList } = this.props;

    return (
      <NewOrderForm
        userList={userList}
        updateOrderInfo={this.updateOrderInfo}
        {...this.state}
      />
    );
  };

  render() {
    return (
      <Dialog
        open={this.props.showModal}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        classes={{ paper: 'modal-container' }}
      >
        <DialogTitle id="alert-dialog-title">
          {this.getModalTitle()}
        </DialogTitle>
        <DialogContent>{this.getModalBody()}</DialogContent>
        <DialogActions>{this.getModalActions()}</DialogActions>
      </Dialog>
    );
  }
}
