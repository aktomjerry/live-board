import React from 'react';
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from '@material-ui/core';
import {orderKeys} from '../constants'; 

export default function NewOrderForm(props) {
  const { selectedUser, orderType, pricePerKg, quantity, userList } = props;

  const userChangeHandler = (e) => {
    props.updateOrderInfo(orderKeys.SELECTED_USER, e.target.value);
  };

  const orderTypeChangeHandler = (e) => {
    props.updateOrderInfo(orderKeys.ORDER_TYPE,e.target.value);
  };

  const priceChangeHandler = (e) => {
    props.updateOrderInfo(orderKeys.PRICE_PER_KG,e.target.value);
  };

  const qtyChangeHandler = (e) => {
    props.updateOrderInfo(orderKeys.QUANTITY,e.target.value);
  };

  return (
    <React.Fragment>
      <FormControl className={'frm-ctrl'}>
        <InputLabel id="demo-simple-select-label">User Id</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={selectedUser}
          onChange={userChangeHandler}
        >
          {userList.map((user) => (
            <MenuItem
              key={user.id}
              value={user.id}
            >{`${user.id} :: ${user.name}`}</MenuItem>
          ))}
        </Select>
      </FormControl>

      <FormControl className={'frm-ctrl'}>
        <TextField
          required
          id="filled-required"
          label="Quantity (kg)"
          variant="standard"
          inputProps={{ type: 'number', min: '0.1' }}
          onInput={qtyChangeHandler}
          value={quantity}
        />
      </FormControl>

      <FormControl className={'frm-ctrl'}>
        <TextField
          required
          id="filled-required"
          label="Price / kg (£)"
          variant="standard"
          inputProps={{ type: 'number', min: '0.1' }}
          onInput={priceChangeHandler}
          value={pricePerKg}
        />
      </FormControl>

      <FormControl className={'frm-ctrl'}>
        <InputLabel id="demo-simple-select-label">Order Type</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={orderType}
          onChange={orderTypeChangeHandler}
        >
          <MenuItem value={'Buy'}> Buy </MenuItem>}
          <MenuItem value={'Sell'}> Sell </MenuItem>}
        </Select>
      </FormControl>
    </React.Fragment>
  );
}
